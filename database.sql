CREATE TABLE mahasiswa
(
    id    INT AUTO_INCREMENT PRIMARY KEY,
    nim   VARCHAR(25)  NOT NULL,
    nama  VARCHAR(100) NOT NULL,
    photo VARCHAR(200) NULL,
    CONSTRAINT mahasiswa_nim_uindex
    UNIQUE (nim)
);
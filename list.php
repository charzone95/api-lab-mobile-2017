<?php

require_once 'connection.php';
/** @var $pdo PDO */

$statement = $pdo->query("SELECT * FROM mahasiswa ORDER BY id DESC");
$statement->execute();

$mahasiswa = $statement->fetchAll();

if (!$mahasiswa) {
    $mahasiswa = [];
}


header('Content-Type: application/json');
echo json_encode($mahasiswa);
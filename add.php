<?php

require_once 'connection.php';
/** @var $pdo PDO */

$nama = @trim(@$_POST['nama']);
$nim = trim(@$_POST['nim']);
$photo = trim(@$_POST['photo']);

$response = [];

if (!$nama || !$nim) {
    $response['success'] = 0;
    $response['message'] = "Nama dan NIM harus ada";
} else {

    if (!$photo) {
        $photo = 'https://portal.usu.ac.id/photos/'. $nim .'.jpg';
    }

    $insertStatement = $pdo->prepare("INSERT INTO mahasiswa(nim, nama, photo) VALUES(:nim, :nama, :photo);");

    $status = $insertStatement->execute([
        ':nim' => $nim,
        ':nama' => $nama,
        ':photo' => $photo
    ]);

    if ($status) {
        $response['success'] = 1;
        $response['message'] = "Data telah disimpan";
    } else {
        $response['success'] = 0;
        $response['message'] = "Gagal menyimpan data";
    }
}

header('Content-Type: application/json');
echo json_encode($response);